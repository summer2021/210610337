import mindspore.ops as ops
import mindspore
from mindspore import nn, Tensor, ParameterTuple
from mindspore.common.initializer import TruncatedNormal
from mindspore.common import dtype as mstype
import numpy as np
from mindspore.common.parameter import Parameter
from mindspore.ops import operations as P
from mindspore.ops import functional as F
from mindspore.ops import composite as C
from mindspore.nn.wrap.grad_reducer import DistributedGradReducer
from mindspore.context import ParallelMode
from mindspore.communication.management import get_group_size
from mindspore import context
from src.CRF import CRF
from src.utils.logger import logger
# from mindspore.ops import Print


# net = nn.LSTM(10, 16, 2, has_bias=True, batch_first=True, bidirectional=False)
# x = Tensor(np.ones([3, 5, 10]).astype(np.float32))
# h0 = Tensor(np.ones([1 * 2, 3, 16]).astype(np.float32))
# c0 = Tensor(np.ones([1 * 2, 3, 16]).astype(np.float32))
# output, (hn, cn) = net(x, (h0, c0))
# print(output.shape)

GRADIENT_CLIP_TYPE = 1
GRADIENT_CLIP_VALUE = 1.0
grad_scale = C.MultitypeFuncGraph("grad_scale")
reciprocal = P.Reciprocal()
@grad_scale.register("Tensor", "Tensor")
def tensor_grad_scale(scale, grad):
    return grad * reciprocal(scale)

_grad_overflow = C.MultitypeFuncGraph("_grad_overflow")
grad_overflow = P.FloatStatus()
@_grad_overflow.register("Tensor")
def _tensor_grad_overflow(grad):
    return grad_overflow(grad)

clip_grad = C.MultitypeFuncGraph("clip_grad")


@clip_grad.register("Number", "Number", "Tensor")
def _clip_grad(clip_type, clip_value, grad):
    """
    Clip gradients.

    Inputs:
        clip_type (int): The way to clip, 0 for 'value', 1 for 'norm'.
        clip_value (float): Specifies how much to clip.
        grad (tuple[Tensor]): Gradients.

    Outputs:
        tuple[Tensor], clipped gradients.
    """
    if clip_type not in (0, 1):
        return grad
    dt = F.dtype(grad)
    if clip_type == 0:
        new_grad = C.clip_by_value(grad, F.cast(F.tuple_to_array((-clip_value,)), dt),
                                   F.cast(F.tuple_to_array((clip_value,)), dt))
    else:
        new_grad = nn.ClipByNorm()(grad, F.cast(F.tuple_to_array((clip_value,)), dt))
    return new_grad

class CustomWithLossCell(nn.Cell):
    def __init__(self, backbone):
        super(CustomWithLossCell, self).__init__(auto_prefix=False)
        self._backbone = backbone

    def construct(self, data, label, mask):
        batch_loss = self._backbone(data, label, mask)
        return batch_loss

class TrainOneStepCell(nn.Cell):
    def __init__(self, network, optimizer, scale_update_cell=None):
        """参数初始化"""
        super(TrainOneStepCell, self).__init__(auto_prefix=False)
        self.network = network
        # 使用tuple包装weight
        self.network.set_grad()
        self.weights = optimizer.parameters
        self.optimizer = optimizer
        # 定义梯度函数
        self.grad = C.GradOperation(get_by_list=True, sens_param=True)
        self.reducer_flag = False
        self.allreduce = P.AllReduce()
        self.parallel_mode = context.get_auto_parallel_context("parallel_mode")
        if self.parallel_mode in [ParallelMode.DATA_PARALLEL, ParallelMode.HYBRID_PARALLEL]:
            self.reducer_flag = True
        self.grad_reducer = None
        if self.reducer_flag:
            mean = context.get_auto_parallel_context("gradients_mean")
            degree = get_group_size()
            self.grad_reducer = DistributedGradReducer(optimizer.parameters, mean, degree)
        self.is_distributed = (self.parallel_mode != ParallelMode.STAND_ALONE)
        self.cast = P.Cast()
        self.gpu_target = False
        if context.get_context("device_target") == "GPU":
            self.gpu_target = True
            self.float_status = P.FloatStatus()
            self.addn = P.AddN()
            self.reshape = P.Reshape()
        else:
            self.alloc_status = P.NPUAllocFloatStatus()
            self.get_status = P.NPUGetFloatStatus()
            self.clear_status = P.NPUClearFloatStatus()
        self.reduce_sum = P.ReduceSum(keep_dims=False)
        self.base = Tensor(1, mstype.float32)
        self.less_equal = P.LessEqual()
        self.hyper_map = C.HyperMap()
        self.loss_scale = None
        self.loss_scaling_manager = scale_update_cell
        if scale_update_cell:
            self.loss_scale = Parameter(Tensor(scale_update_cell.get_loss_scale(), dtype=mstype.float32))

    def construct(self, data, label, mask, sens=None):
        """构建训练过程"""
        weights = self.weights
        init = False
        loss = self.network(data,
                            label,
                            mask)
        if sens is None:
            scaling_sens = self.loss_scale
        else:
            scaling_sens = sens

        if not self.gpu_target:
            init = self.alloc_status()
            init = F.depend(init, loss)
            clear_status = self.clear_status(init)
            scaling_sens = F.depend(scaling_sens, clear_status)
        grads = self.grad(self.network, weights)(data,
                                                 label,
                                                 mask,
                                                 self.cast(scaling_sens,
                                                           mstype.float32))
        grads = self.hyper_map(F.partial(grad_scale, scaling_sens), grads)
        grads = self.hyper_map(F.partial(clip_grad, GRADIENT_CLIP_TYPE, GRADIENT_CLIP_VALUE), grads)
        if self.reducer_flag:
            grads = self.grad_reducer(grads)
        if not self.gpu_target:
            init = F.depend(init, grads)
            get_status = self.get_status(init)
            init = F.depend(init, get_status)
            flag_sum = self.reduce_sum(init, (0,))
        else:
            flag_sum = self.hyper_map(F.partial(_grad_overflow), grads)
            flag_sum = self.addn(flag_sum)
            flag_sum = self.reshape(flag_sum, (()))
        if self.is_distributed:
            flag_reduce = self.allreduce(flag_sum)
            cond = self.less_equal(self.base, flag_reduce)
        else:
            cond = self.less_equal(self.base, flag_sum)
        overflow = cond
        if sens is None:
            overflow = self.loss_scaling_manager(self.loss_scale, cond)
        if overflow:
            succ = False
        else:
            succ = self.optimizer(grads)
        ret = (loss, cond)
        return F.depend(ret, succ)

class BiLSTMCRF(nn.Cell):
    def __init__(self, embedding_size, hidden_size, bilstm_layer_num, tag_to_index_dict, word_to_index_dict, batch_size, seq_len, is_training) -> None:
        super().__init__()
        self.embedding_size = embedding_size
        self.batch_size = batch_size
        self.hidden_size = hidden_size
        self.is_training = is_training
        self.bilstm_layer_num = bilstm_layer_num
        self.tag_to_index_dict = tag_to_index_dict
        self.initializer_range = 0.02
        # self.debug = Print()
        self.weight_init = TruncatedNormal(self.initializer_range)
        self.transpose = ops.Transpose()
        self.expanddims = ops.ExpandDims()
        self.type_cast_float32 = ops.Cast()
        self.embedding = nn.Embedding(len(word_to_index_dict), embedding_size)
        self.h0 = Tensor(np.zeros((self.bilstm_layer_num * 2, self.batch_size, self.hidden_size)).astype(np.float32))
        self.c0 = Tensor(np.zeros((self.bilstm_layer_num * 2, self.batch_size, self.hidden_size)).astype(np.float32))
        self.bilstm = nn.LSTM(self.embedding_size, self.hidden_size, self.bilstm_layer_num, has_bias=True, bidirectional=True)
        self.dense_1 = nn.Dense(self.hidden_size * 2, len(tag_to_index_dict), weight_init=self.weight_init,
                                has_bias=True).to_float(mstype.float32)
        self.crf = CRF(tag_to_index_dict, batch_size=self.batch_size, seq_length=seq_len, is_training=self.is_training)
    
    def construct(self, features, labels, masks):
        # self.debug("Network input feature shape: ", features.shape())
        # self.debug("Network input labels shape: ", labels.shape())
        masks = self.type_cast_float32(masks, mindspore.float32)

        embeds = self.embedding(features)
        embeds = self.type_cast_float32(embeds, mindspore.float32)
        masked_embeds = embeds * self.expanddims(masks, 2)
        batch_sec_embeds = self.transpose(embeds, (1,0,2))
        # self.debug("Network embeds shape: ", embeds.shape())
        logits, (hn, cn) = self.bilstm(batch_sec_embeds, (self.h0, self.c0))
        # self.debug("Network output logits shape: ", logits.shape())
        # need transpose
        batch_first_logits = self.transpose(logits, (1,0,2))
        logits_dense = self.dense_1(batch_first_logits)
        # self.debug("Network dense layer logits shape:", logits.shape())
        loss = self.crf(logits_dense, labels)
        # self.debug("loss function: ", loss)
        return loss