from mindspore import context
from mindspore import dataset as ds
from mindspore.nn.optim import Momentum, Adam
from mindspore import Model
from mindspore.train.callback import CheckpointConfig, ModelCheckpoint, TimeMonitor
import mindspore.dataset.transforms.c_transforms as C
from mindspore.train.serialization import load_checkpoint, load_param_into_net
from mindspore import dtype as mstype
from src.utils.tokenizer import conll2003_tag_tokenizer, conll2003_tokenizer
from src.model import BiLSTMCRF, CustomWithLossCell, TrainOneStepCell
from mindspore.nn.wrap.loss_scale import DynamicLossScaleUpdateCell
from src.data_generators.conll2003 import conll2003_train_dataset, conll2003_test_dataset
from src.utils.train_utils import LossCallBack
from src.utils.logger import ckpt_logger
from .eval import F1WithCRF
import time

def get_train_dataset(batch_size):
    train_data = ds.GeneratorDataset(conll2003_train_dataset, column_names=['features','labels', 'masks'])
    # dataset type cast
    type_cast_op_labels = C.TypeCast(mstype.int32)
    type_cast_op_masks = C.TypeCast(mstype.float32)
    train_data = train_data.map(operations=type_cast_op_labels, input_columns="labels")
    train_data = train_data.map(operations=type_cast_op_labels, input_columns="features")
    train_data = train_data.map(operations=type_cast_op_masks, input_columns="masks")
    train_data = train_data.batch(batch_size)
    return train_data

def get_test_dataset(batch_size=1):
    test_data = ds.GeneratorDataset(conll2003_test_dataset, column_names=['features','labels', 'masks'])
    type_cast_op_labels = C.TypeCast(mstype.int32)
    type_cast_op_masks = C.TypeCast(mstype.float32)
    test_data = test_data.map(operations=type_cast_op_labels, input_columns="labels")
    test_data = test_data.map(operations=type_cast_op_labels, input_columns="features")
    test_data = test_data.map(operations=type_cast_op_masks, input_columns="masks")
    test_data = test_data.batch(batch_size)
    return test_data

def run_eval():
    load_ckpt_path = "./conll_ner_finetune/ckpt/"
    embedding_size = 128
    hidden_size = 384
    batch_size = 12
    seq_len = conll2003_test_dataset.get_max_length()
    tag_to_index_dict = conll2003_tag_tokenizer.get_dict()[0]
    word_to_index_dict = conll2003_tokenizer.get_dict()[0]
    max_val = max(tag_to_index_dict.values())
    num_classes = len(tag_to_index_dict)
    tag_to_index_dict["<START>"] = max_val + 1
    tag_to_index_dict["<STOP>"] = max_val + 2
    pretrained_net = BiLSTMCRF(embedding_size=embedding_size, hidden_size=hidden_size, bilstm_layer_num=2, tag_to_index_dict=tag_to_index_dict, word_to_index_dict=word_to_index_dict, batch_size=batch_size, seq_len=seq_len, is_training=False)
    pretrained_net.set_train(False)
    param_dict = load_checkpoint(load_ckpt_path)
    load_param_into_net(pretrained_net, param_dict)
    model = Model(pretrained_net)
    callback = F1WithCRF(num_classes)
    # fetch test data
    ds = get_test_dataset()
    columns_list = ["labels", "features", "masks"]
    for data in ds.create_dict_iterator(num_epochs=1):
        input_data = []
        for i in columns_list:
            input_data.append(data[i])
        labels, features, masks = input_data
        logits = model.predict(labels, features, masks)
        callback.update(logits, labels)
    ckpt_logger.info("Evaluation Result")
    ckpt_logger.info("Precision {:.6f} ".format(callback.TP / (callback.TP + callback.FP)))
    ckpt_logger.info("Recall {:.6f} ".format(callback.TP / (callback.TP + callback.FN)))
    ckpt_logger.info("F1 {:.6f} ".format(2 * callback.TP / (2 * callback.TP + callback.FP + callback.FN)))


def run_train(pretrain = False):
    # hyperparameters
    epoch_num = 500
    save_ckpt_path = "./conll_ner_finetune/ckpt/"
    pretrain_load_ckpt_path = "./pretrain/step_1.ckpt"
    device_id = 0
    embedding_size = 128
    hidden_size = 384
    momentum_learning_rate = 0.00002
    learning_rate = 0.00005
    if pretrain:
        learning_rate = 0.0001
    momentum = 0.9
    batch_size = 12
    seq_len = conll2003_train_dataset.get_max_length()
    context.set_context(mode=context.GRAPH_MODE, device_target="Ascend", device_id=device_id, max_call_depth=2000)
    # context.set_context(mode=context.PYNATIVE_MODE, device_target="Ascend", device_id=device_id, max_call_depth=2000)
    tag_to_index_dict = conll2003_tag_tokenizer.get_dict()[0]
    word_to_index_dict = conll2003_tokenizer.get_dict()[0]
    max_val = max(tag_to_index_dict.values())
    tag_to_index_dict["<START>"] = max_val + 1
    tag_to_index_dict["<STOP>"] = max_val + 2
    netwithloss = BiLSTMCRF(embedding_size=embedding_size, hidden_size=hidden_size, bilstm_layer_num=2, tag_to_index_dict=tag_to_index_dict, word_to_index_dict=word_to_index_dict, batch_size=batch_size, seq_len=seq_len, is_training=True)
    train_data = get_train_dataset(batch_size)
    steps_per_epoch = train_data.get_dataset_size()
    optimizer = Adam(netwithloss.trainable_params(), learning_rate=learning_rate, weight_decay=0.1)
    ckpt_config = CheckpointConfig(save_checkpoint_steps=steps_per_epoch, keep_checkpoint_max=10)
    ckpoint_cb = ModelCheckpoint(prefix="conll2003",
                                 directory=save_ckpt_path,
                                 config=ckpt_config)
    custom_cell = CustomWithLossCell(netwithloss)
    update_cell = DynamicLossScaleUpdateCell(loss_scale_value=2**32, scale_factor=2, scale_window=1000)
    train_cell = TrainOneStepCell(custom_cell, optimizer, update_cell)
    if pretrain:
        param_dict = load_checkpoint(pretrain_load_ckpt_path)
        load_param_into_net(train_cell, param_dict)
    model = Model(train_cell)
    callbacks = [TimeMonitor(train_data.get_dataset_size()), LossCallBack(train_data.get_dataset_size()), ckpoint_cb]
    train_begin = time.time()
    model.train(epoch=epoch_num, train_dataset=train_data, callbacks=callbacks)
    train_end = time.time()
    print("latency: {:.6f} s".format(train_end - train_begin))

if __name__ == "__main__":
    run_train(pretrain=False)
    # run_eval()