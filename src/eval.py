import numpy as np
from src.CRF import postprocess
from src.utils.metrics import ConfusionMat

class F1WithCRF():
    '''
    calculate F1 score
    '''
    def __init__(self, num_labels=2):
        self.TP = 0
        self.FP = 0
        self.FN = 0
        self.num_labels = num_labels
        self.metric = ConfusionMat(self.num_labels)

    def mat(self):
        return self.metric.mat()

    def update(self, logits, labels):
        '''
        update F1 score
        '''
        labels = labels.asnumpy()
        labels = np.reshape(labels, -1)
        backpointers, best_tag_id = logits
        best_path = postprocess(backpointers, best_tag_id)
        logit_id = []
        for ele in best_path:
            logit_id.extend(ele)

        target = np.zeros((len(labels), self.num_labels), dtype=np.int)
        pred = np.zeros((len(logit_id), self.num_labels), dtype=np.int)
        for i, label in enumerate(labels):
            target[i][label] = 1
        for i, label in enumerate(logit_id):
            pred[i][label] = 1
        self.metric.update(pred, target)

    def eval(self):
        return self.metric.eval()

    def f1(self):
        return self.metric.eval()
    
    def precision(self):
        return self.metric.precision()

    def recall(self):
        return self.metric.recall()
