from mindspore import nn
from mindspore import dataset as ds
from mindspore import dtype as mstype
import mindspore.dataset.transforms.c_transforms as C
from src.data_generators.conll2003 import conll2003_train_dataset
from src.utils.tokenizer import conll2003_tag_tokenizer, conll2003_tokenizer

def get_train_dataset(batch_size):
    train_data = ds.GeneratorDataset(conll2003_train_dataset, column_names=['features','labels', 'masks'])
    # dataset type cast
    type_cast_op_labels = C.TypeCast(mstype.int32)
    type_cast_op_masks = C.TypeCast(mstype.float32)
    train_data = train_data.map(operations=type_cast_op_labels, input_columns="labels")
    train_data = train_data.map(operations=type_cast_op_labels, input_columns="features")
    train_data = train_data.map(operations=type_cast_op_masks, input_columns="masks")
    train_data = train_data.batch(batch_size)
    return train_data

net = nn.Embedding(46, 128) # True
train_data = get_train_dataset(12)
print(len(conll2003_tokenizer.get_dict()[0]))
print(len(conll2003_tag_tokenizer.get_dict()[0]))
for i in train_data:
    print(i[0])
    output = net(i[0])
    result = output.shape
    print(result)