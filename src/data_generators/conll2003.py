from src.utils.config import parse_yaml
import pathlib
import numpy as np
import pandas as pd
from src.utils.tokenizer import conll2003_tag_tokenizer, conll2003_tokenizer
from src.utils.logger import logger
# read in config
config_yaml_path = "./src/data_prep/pre_config_conll2003.yaml" # need to be turned into argument
cfg, _, _ = parse_yaml(config_yaml_path)

class CoNLL2003Generator:
    # TODO: split data into 128 sperate parts
    def retrieve_data(self, part, max_len):
        train_path = pathlib.Path(cfg["output_path"]) / (part + ".txt")
        self.data = []
        self.label = []
        self.mask = []
        with train_path.open() as train_f:
            train_list = train_f.readlines()
            train_list = list(filter(lambda x: x != "", map(lambda x: x.strip(), train_list)))
            train_data_list = train_list[::2]
            train_label_list = train_list[1::2]
            for data in train_data_list:
                data_all_len = list(map(lambda x: conll2003_tokenizer.tokenize(x), filter(lambda x: x != "", data.split(" "))))
                if len(data_all_len) <= max_len:
                    self.data.append(data_all_len)
            for label in train_label_list:
                label_all_len = list(map(lambda x: conll2003_tag_tokenizer.tokenize(x), filter(lambda x: x != "", label.split(" "))))
                if len(label_all_len) <= max_len:
                    self.label.append(label_all_len)
            # self.data = pd.DataFrame(self.data)
            # self.label = pd.DataFrame(self.label)
            assert(len(self.data) == len(self.label))
            self.max_length = max_len
            for index in range(0, len(self.data)):
                assert len(self.data[index]) == len(self.label[index])
                self.data[index].extend([0] * (self.max_length - len(self.data[index])))
                self.label[index].extend([0] * (self.max_length - len(self.label[index])))
                data = self.data[index]
                label = self.label[index]
                assert(len(data) == len(label))
            logger.warning("conll2003 data loaded")
        # logger.warn(self.max_length)
        for label_data in self.label:
            mask_data = [1] * len(label_data)
            mask_data.extend([0] * (self.max_length - len(label_data)))
            self.mask.append(mask_data)

    def __getitem__(self, index):
        data_arr = np.array(self.data[index])
        # data_arr = np.expand_dims(data_arr, axis=1)
        label_arr = np.array(self.label[index])
        # label_arr = np.expand_dims(label_arr, axis=1)
        mask_arr = np.array(self.mask[index])
        # logger.warn(data_arr.shape)
        # logger.warn(mask_arr.shape)
        # logger.warn(label_arr.shape)
        assert data_arr.shape == label_arr.shape
        assert label_arr.shape == mask_arr.shape
        return data_arr, label_arr, mask_arr
    
    def __len__(self):
        return len(self.data)

    def get_max_length(self):
        return self.max_length

    def __init__(self, part, max_len) -> None:
        self.retrieve_data(part, max_len)

max_len = 128
conll2003_train_dataset = CoNLL2003Generator("train", max_len)
conll2003_test_dataset = CoNLL2003Generator("test", max_len)
conll2003_valid_dataset = CoNLL2003Generator("valid", max_len)