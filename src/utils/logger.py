import logging
# TODO: change to global config
logging_path = "./debug.log"
logger = logging.getLogger("debug")
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(logging_path, encoding="utf-8")
ch = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(filename)s - [line %(lineno)d] - %(message)s')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)

ckpt_logging_path = "./ckpt_training.log"
ckpt_logger = logging.getLogger("ckpt_training")
ckpt_logger.setLevel(logging.INFO)
ckpt_fh = logging.FileHandler(ckpt_logging_path, encoding="utf-8")
ckpt_ch = logging.StreamHandler()
ckpt_fh.setLevel(logging.INFO)
ckpt_fh.setFormatter(formatter)
ckpt_ch.setFormatter(formatter)
ckpt_logger.addHandler(ckpt_fh)
ckpt_logger.addHandler(ckpt_ch)