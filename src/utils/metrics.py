import numpy as np
class ConfusionMat():
    def __init__(self, num_class) -> None:
        self.num_class = num_class
        self.TP = np.zeros(self.num_class, np.int)
        self.FP = np.zeros(self.num_class, np.int)
        self.FN = np.zeros(self.num_class, np.int)
    
    def update(self, pred, target) -> None:
        assert pred.shape[1] == target.shape[1]
        assert pred.shape[1] == self.num_class
        pred_t = np.transpose(pred, (1,0))
        target_t = np.transpose(target, (1,0))
        # print(pred_t)
        # print(target_t)
        TP_bool = np.logical_and((pred_t == target_t), (pred_t == 1))
        TP_int = np.where(TP_bool == True, 1, 0)
        # print(TP_int)
        TP_label_wise = np.sum(TP_int, axis=1)
        # print(TP_label_wise)
        self.TP = self.TP + TP_label_wise
        # print(self.TP)
        neg_target_t = 1 - target_t
        FP_bool = np.logical_and((pred_t == neg_target_t), (pred_t == 1))
        FP_int = np.where(FP_bool == True, 1, 0)
        FP_label_wise = np.sum(FP_int, axis=1)
        self.FP = self.FP + FP_label_wise
        neg_pred_t = 1- pred_t
        FN_bool = np.logical_and((target_t == neg_pred_t), (pred_t == 0))
        FN_int = np.where(FN_bool == True, 1, 0)
        FN_label_wise = np.sum(FN_int, axis=1)
        self.FN = self.FN + FN_label_wise
        # print(self.TP)
        # print(self.FP)
        # print(self.FN)

    def mat(self):
        return self.TP, self.FP, self.FN

    def eval(self):
        self.precision = np.zeros_like(self.TP, np.float)
        np.divide(self.TP, (self.TP + self.FP), out=self.precision, where=(self.TP + self.FP) != 0)
        self.recall = np.zeros_like(self.TP, np.float)
        np.divide(self.TP, (self.TP + self.FN), out=self.recall, where=(self.TP + self.FN) != 0)
        self.multi_f1 = np.zeros_like(self.precision, np.float)
        np.divide(2 * (self.precision * self.recall), (self.precision + self.recall), out=self.multi_f1, where=((self.precision + self.recall) != 0))
        self.multi_f1 = np.where(np.logical_and.reduce([self.TP == 0, self.FP == 0, self.FN == 0]), 1, self.multi_f1)
        # print(self.multi_f1)
        return np.mean(self.multi_f1)
    
    def precision(self):
        self.precision = np.zeros_like(self.TP, np.float)
        np.divide(self.TP, (self.TP + self.FP), out=self.precision, where=(self.TP + self.FP) != 0)
        return np.mean(self.precision)
    
    def recall(self):
        self.recall = np.zeros_like(self.TP, np.float)
        np.divide(self.TP, (self.TP + self.FN), out=self.recall, where=(self.TP + self.FN) != 0)
        return np.mean(self.recall)

if __name__ == "__main__":
    metric = ConfusionMat(3)
    test_data_pred = np.array([[0,1,0], [1, 0, 0], [0, 0, 1]], np.int)
    test_data_target = np.array([[1,0,0], [1,0,0], [0,0,1]], np.int)
    #TP 1 FN 1 FP 0 # TP 0 FN 0 FP 1 # TP 1 FN 0 FP 0
    #p 1 r 0.5 # p 0 r 0 # p 1 r 1
    # f1 2/3 # f1 0 # f1 1
    metric.update(test_data_pred, test_data_target)
    test_data_pred = np.array([[1,0,0], [1, 0, 0], [0, 0, 1]], np.int)
    test_data_target = np.array([[1,0,0], [1,0,0], [0,0,1]], np.int)
    #TP 3 FN 1 FP 0 # TP 0 FN 0 FP 1 # TP 2 FN 0 FP 0
    #p 1 r 3/4 # p 0 r 0 # p 1 r 1
    # f1 6/7 # f1 0 # f1 1
    metric.update(test_data_pred, test_data_target)
    print(metric.eval())