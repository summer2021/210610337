import pathlib
import collections
class Tokenizer():
    def __init__(self, vocab_file_path) -> None:
        vocab_path = pathlib.Path(vocab_file_path)
        self.vocab = collections.OrderedDict()
        self.reverse_vocab = collections.OrderedDict()
        with vocab_path.open() as vocab_f:
            vocab_data = vocab_f.readlines()
            vocab_list = map(lambda x: x.strip(), vocab_data)
            vocab_list = filter(lambda x: x != "", vocab_list)
            index = 1
            for word in vocab_list:
                self.vocab[word] = index
                self.reverse_vocab[index] = word
                index += 1
        self.vocab["<UNK>"] = 0
        self.reverse_vocab[0] = "<UNK>"

    def tokenize(self, text):
        return self.vocab.get(text, 0)

    def reverse(self, id):
        return self.reverse_vocab.get(id, "<UNK>")

    def get_dict(self):
        return dict(self.vocab), dict(self.reverse_vocab)

conll2003_vocab_path = "./src/data_prep/generated/conll2003/vocab.txt"
conll2003_tag_path = "./src/data_prep/generated/conll2003/tag_vocab.txt"
conll2003_tokenizer = Tokenizer(conll2003_vocab_path)
conll2003_tag_tokenizer = Tokenizer(conll2003_tag_path)