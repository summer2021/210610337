from src.utils.tokenizer import conll2003_tokenizer, conll2003_tag_tokenizer
import pathlib
from src.utils.config import parse_yaml
# TODO: complete this

# read in config
config_yaml_path = "./src/data_prep/pre_config_conll2003.yaml" # need to be turned into argument
cfg, _, _ = parse_yaml(config_yaml_path)

train_path = pathlib.Path(cfg["output_path"]) / "train.txt"
test_path = pathlib.Path(cfg["output_path"]) / "test.txt"
valid_path = pathlib.Path(cfg["output_path"]) / "valid.txt"
output_train_path = pathlib.Path(cfg["output_path"]) / "train.txt"
output_test_path = pathlib.Path(cfg["output_path"]) / "test.txt"
output_valid_path = pathlib.Path(cfg["output_path"]) / "valid.txt"

def deposit_data_file(in_path, out_path):